import logging
import os

import torch
from fvcore.common.checkpoint import Checkpointer
from fvcore.common.config import CfgNode
from torch import distributed as dist
from torch.nn import functional as F
from torch.nn.parallel import DistributedDataParallel
from torch.utils.tensorboard import SummaryWriter

from src.data import build_loaders
from src.model import build_model
from src.model.base import BaseModel


class Trainer:
    """Trainer object with a simple training procedure:

    1. Initialize the model, dataloaders, optimizer, scheduler, and checkpointer with
       the given `cfg`.
    2. Load the model from the latest checkpoint if the --resume flag is set.
    3. Run noise2noise training and save the latest and best model with the lowest MSE.
    """

    def __init__(self, cfg: CfgNode) -> object:
        """
        :param CfgNode cfg: Configuration object with training hyper-parameters.
        """
        logger = logging.getLogger("denoise")

        self.rank = dist.get_rank() if dist.is_initialized() else 0
        self.world_size = dist.get_world_size() if dist.is_initialized() else 1

        self.train_loader, self.val_loader = build_loaders(cfg)
        logger.info(f"training:   {len(self.train_loader.dataset)} sample(s)")
        logger.info(f"validation: {len(self.val_loader.dataset)} sample(s)")

        self.model = self.build_model(cfg)
        logger.info(f"Model:\n{str(self.model)}")

        opt_params = dict(cfg.train.optimizer)
        opt_name = opt_params.pop("name")
        opt_class = getattr(torch.optim, opt_name)
        self.optimizer = opt_class(self.model.parameters(), **opt_params)
        logger.info(
            f"{opt_name}({', '.join([f'{k}={v}' for k, v in opt_params.items()])})"
        )

        if cfg.train.get("scheduler", None) is not None:
            sch_params = dict(cfg.train.scheduler)
            sch_name = sch_params.pop("name")
            self.scheduler = getattr(torch.optim.lr_scheduler, sch_name)(
                self.optimizer, **sch_params
            )
            logger.info(
                f"{sch_name}({', '.join([f'{k}={v}' for k, v in sch_params.items()])})"
            )
        else:
            self.scheduler = None

        ckptr_modules = dict(optimizer=self.optimizer)
        if self.scheduler is not None:
            ckptr_modules["scheduler"] = self.scheduler
        self.checkpointer = Checkpointer(
            self.model.module if self.world_size > 1 else self.model,
            cfg.train.output_dir,
            **ckptr_modules,
        )
        self.ckpt_freq = cfg.train.get("ckpt_freq", float("inf"))

        self.start_epoch = 0
        self.end_epoch = cfg.train.epochs
        if cfg.train.resume:
            ckpt_path = os.path.join(self.checkpointer.save_dir, "model_latest.pth")
            state_dict = self.checkpointer.resume_or_load(ckpt_path)
            self.start_epoch = state_dict.get("epoch", 0) + 1

        if self.rank == 0:
            self.writer = SummaryWriter(log_dir=cfg.train.output_dir)

        if torch.cuda.is_available():
            self.device = torch.device(f"cuda:{self.rank}")
        else:
            self.device = torch.device("cpu")

    @classmethod
    def build_model(cls, cfg: CfgNode) -> BaseModel:
        """
        Build a model with the given configuration object.

        :param CfgNode cfg: Configuration object with model specifications.
        :return BaseModel: The constructed model.
        """
        model = build_model(cfg)
        if torch.cuda.is_available():
            model.cuda()
        if dist.is_initialized() and dist.get_world_size() > 1:
            model = DistributedDataParallel(
                model, device_ids=[dist.get_rank()], broadcast_buffers=False
            )
        return model

    def train(self):
        """The training logic."""
        logger = logging.getLogger("denoise")

        for epoch in range(self.start_epoch, self.end_epoch):
            if self.world_size > 1:
                self.train_loader.sampler.set_epoch(epoch)
                self.val_loader.sampler.set_epoch(epoch)

            metrics = dict(train_loss=0, val_mse=0)

            # training
            self.model.train()
            for x, y in self.train_loader:
                x = x.to(self.device)
                y = y.to(self.device)
                _, loss = self.model(x, y)

                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                with torch.no_grad():
                    metrics["train_loss"] += loss.item() * x.size(0)

            # validation
            self.model.eval()
            for x, y in self.val_loader:
                x = x.to(self.device)
                y = y.to(self.device)
                with torch.no_grad():
                    p = self.model(x)
                    metrics["val_mse"] += F.mse_loss(p, y).item() * x.size(0)

            if self.scheduler is not None:
                if isinstance(
                    self.scheduler, torch.optim.lr_scheduler.ReduceLROnPlateau
                ):
                    self.scheduler.step(metrics["val_mse"])
                else:
                    self.scheduler.step()

            if self.world_size > 1:
                for k, v in metrics.items():
                    v = torch.as_tensor(v).to(self.device)
                    dist.reduce(v, dst=0, op=dist.ReduceOp.SUM)
                    metrics[k] = v.item()

            for k in metrics:
                if k.startswith("train"):
                    metrics[k] /= len(self.train_loader.dataset)
                else:
                    metrics[k] /= len(self.val_loader.dataset)

            logger.info(
                f"Epoch [{epoch + 1:03d}/{self.end_epoch}] "
                + " - ".join([f"{k}: {v:.4f}" for k, v in sorted(metrics.items())])
                + f" - lr: {self.optimizer.param_groups[0]['lr']:.1e}"
            )
            if self.rank == 0:
                for k, v in metrics.items():
                    self.writer.add_scalar(k, v, epoch)

                if (epoch + 1) % self.ckpt_freq == 0:
                    self.checkpointer.save(f"model_epoch{epoch + 1:03d}")

                if metrics["val_mse"] < globals().get("best_score", float("inf")):
                    self.checkpointer.save(f"model_best")
                    globals()["best_score"] = metrics["val_mse"]

                self.checkpointer.save(f"model_latest")

        if self.rank == 0:
            self.checkpointer.save(f"model_final")
