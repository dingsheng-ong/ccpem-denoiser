import logging
import os
import random
import sys
from datetime import datetime
from functools import lru_cache

import numpy as np
import torch
from termcolor import colored


def seed_all_rng(seed: int = -1):
    """
    Seeds all random number generators from Python, NumPy, and PyTorch for consistency
    and reproducibility. If -1 is provided, a random seed will be generated.

    :param int seed: The RNG seed. If set to -1, a random seed will be generated
        (default: -1).
    """
    if seed < 0:
        seed = (
            os.getpid()
            + int(datetime.now().strftime("%S%f"))
            + int.from_bytes(os.urandom(2), "big")
        )
        logger = logging.getLogger("t")
        logger.info(f"Using generated seed {seed}")

    np.random.seed(seed)
    torch.manual_seed(seed)
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)


def setup_logger(
    output_dir: str, rank: int, name: str = "run", level: logging = logging.DEBUG
) -> logging.Logger:
    """
    Sets up and returns a logger to record training details. Logs are saved in
    `output_dir` with the filename `log.txt`. In multi-GPU settings, only the log from
    rank 0 is displayed on stdout, while logs from other ranks are saved as
    `log.txt.rank[N]`.

    :param str output_dir: Directory to save the log files.
    :param int rank: Rank of the process (for multi-GPU setups).
    :param str name: Name of the logger (default: "run").
    :param logging level: Logging level (default: logging.DEBUG).
    :return logging.Logger: Configured logger instance.
    """
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.propagate = False

    formatter = logging.Formatter(
        fmt=colored("[%(asctime)s %(name)s]: ", "green") + "%(message)s",
        datefmt="%m/%d %H:%M:%S",
    )

    if rank == 0:
        ch = logging.StreamHandler(stream=sys.stdout)
        ch.setLevel(level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    output = os.path.join(output_dir, "log.txt")
    if rank > 0:
        output += f".rank{rank}"

    os.makedirs(os.path.dirname(output), exist_ok=True)
    fh = logging.FileHandler(output, mode="a", encoding="utf-8")
    fh.setFormatter(formatter)
    fh.setLevel(level)
    logger.addHandler(fh)

    return logger
