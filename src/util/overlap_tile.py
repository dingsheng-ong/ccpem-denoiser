import numpy as np
import torch
from torch import distributed as dist
from torch import nn
from torch.nn import functional as F
from tqdm import tqdm


def overlap_tile_inference(
    data: torch.Tensor,  # batch x channel x row x col x sec
    model: nn.Module,
    tile_size: int = 64,
    batch_size: int = 1,
    overlap_size: int = 4,
) -> torch.Tensor:
    """
    Splits the full tomogram (`data`) into overlapping tiles and runs denoising on the
    batched tiles using the given `model`. Then, reconstructs the tiles into a full
    tomogram and returns it. The `tile_size`, `batch_size`, and `overlap_size` determine
    how the tiles are divided.

    :param torch.Tensor data: The input tomogram data with shape
        (batch, channel, row, col, sec).
    :param nn.Module model: The model used for denoising.
    :param int tile_size: Size of each tile (default: 64).
    :param int batch_size: Batch size for denoising (default: 16).
    :param int overlap_size: Overlap size for splitting tiles (default: 8).
    :return torch.Tensor: The denoised full tomogram.
    """
    assert model.training is False

    rank = dist.get_rank() if dist.is_initialized() else 0
    world_size = dist.get_world_size() if dist.is_initialized() else 1

    ov = overlap_size
    if rank == 0:
        _, _, *size = data.shape

        pad_sizes = []
        for x in size:
            n = int(np.ceil(x / (tile_size - 2 * ov)))
            y = (tile_size - 2 * ov) * n
            p = y - x
            pad_sizes = pad_sizes + [p // 2, p - p // 2]

        data = F.pad(data, pad_sizes[::-1], mode="replicate")
        padded = F.pad(data, (ov,) * 6, mode="replicate")
        tiles = padded
        for i in range(3):
            assert (
                tile_size - 2 * ov > 0
            ), f"tile size must be greater than {2 * ov} ({tile_size})"
            tiles = tiles.unfold(i + 2, tile_size, tile_size - 2 * ov)

        n_tiles_per_dim = tiles.shape[2:5]
        tiles = tiles.contiguous().view(1, -1, *(tile_size,) * 3).transpose(0, 1)

    if world_size > 1:
        if rank == 0:
            tiles = tiles.split(int(np.ceil(tiles.shape[0] / world_size)), dim=0)
        else:
            tiles = [None for _ in range(world_size)]

        output_list = [None]
        dist.scatter_object_list(output_list, tiles, src=0)
        tiles = output_list[0]

    output_tiles = []
    if rank == 0:
        loader = tqdm(
            tiles.split(batch_size, dim=0),
            total=int(np.ceil(tiles.shape[0] / batch_size)),
        )
    else:
        loader = tiles.split(batch_size, dim=0)
    for x in loader:
        if torch.cuda.is_available():
            x = x.cuda()
        with torch.no_grad():
            y = model(x)[(...,) + (slice(ov, -ov),) * 3]
            output_tiles.append(y.detach().cpu())
    output_tiles = torch.cat(output_tiles, dim=0)

    if world_size > 1:
        output_list = [None for _ in range(world_size)]
        dist.gather_object(output_tiles, output_list if rank == 0 else None, dst=0)
        if rank == 0:
            output_tiles = torch.cat(output_list, dim=0)

    if rank == 0:
        output = output_tiles.transpose(0, 1).view(
            1, 1, *n_tiles_per_dim, *output_tiles.shape[-3:]
        )
        output = output.contiguous().permute(0, 1, 2, 5, 3, 6, 4, 7)
        output = output.reshape(data.shape)
        return output[
            (...,)
            + tuple(
                slice(b, s if a == 0 else -a)
                for s, a, b in zip(size, pad_sizes[::2], pad_sizes[1::2])
            )
        ]
    else:
        return None
