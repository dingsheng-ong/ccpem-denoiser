from abc import ABCMeta
from typing import Tuple

from fvcore.common.config import CfgNode
from fvcore.common.registry import Registry
from torch import distributed as dist
from torch.utils.data import DataLoader, Dataset
from torch.utils.data.distributed import DistributedSampler

DATASET_REGISTRY = Registry("DATASET")
DATASET_REGISTRY.__doc__ = """
Registry for all dataset objects.

All registered objects must be callable and return an instance of :class:`BaseDataset`.
Datasets can be identified by `cfg.data.name`, and the parameters to initialize the
dataset should be specified in the `cfg.data` configuration object.
"""


class BaseDataset(Dataset, metaclass=ABCMeta):
    """
    Abstract base class for dataset objects.

    The dataset object must implement the `__getitem__` method, which takes a single
    integer argument and returns a pair of training samples. The training pair must have
    the same size and must be a 4-D array/tensor with shape: 1 x row x col x sec.
    """

    def __init__(self) -> Dataset:
        """The subclass can specify its own arguments for the `__init__` method."""
        super().__init__()


def build_loaders(cfg: CfgNode) -> Tuple[DataLoader, DataLoader]:
    """
    Builds and returns a train loader and a validation data loader based on the provided
    `cfg.data`.

    The dataset to use is specified by `cfg.data.name`, and the parameters required to
    build the dataset are defined in `cfg.data`. Some parameters for the data loaders,
    such as `batch_size` and `num_workers`, are defined in `cfg.train`.

    :param CfgNode cfg: Configuration object containing data settings.
    :return Tuple[DataLoader, DataLoader]: A tuple containing the train data loader and
        the validation data loader.
    """
    rank = dist.get_rank() if dist.is_initialized() else 0
    world_size = dist.get_world_size() if dist.is_initialized() else 1

    dataset_config = dict(cfg.data)
    dataset_fn = DATASET_REGISTRY.get(dataset_config.pop("name"))

    train_dataset = dataset_fn(is_train=True, **dataset_config)
    val_dataset = dataset_fn(is_train=False, **dataset_config)
    assert isinstance(train_dataset, BaseDataset)
    assert isinstance(val_dataset, BaseDataset)

    if world_size > 1:
        train_sampler = DistributedSampler(train_dataset, world_size, rank=rank)
        val_sampler = DistributedSampler(val_dataset, world_size, rank=rank)
    else:
        train_sampler = val_sampler = None

    train_loader = DataLoader(
        train_dataset,
        batch_size=cfg.train.batch_size,
        num_workers=cfg.train.get("num_workers", 1),
        sampler=train_sampler,
    )
    val_loader = DataLoader(
        val_dataset,
        batch_size=cfg.train.batch_size,
        num_workers=cfg.train.get("num_workers", 1),
        sampler=val_sampler,
    )

    return train_loader, val_loader
