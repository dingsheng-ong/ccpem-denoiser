from typing import Tuple, Union

import torch
from torch import nn
from torch.nn import functional as F

from .base import MODEL_REGISTRY, BaseModel


@MODEL_REGISTRY.register()
class VAE3d(BaseModel):
    """Implement the 3D Variational AutoEncoder."""

    def __init__(
        self,
        nch_in: int = 1,
        nch_out: int = 1,
        latent_dim: int = 128,
        kld_weight: float = 2.5e-4,
        nch_feats: int = 32,
        ksize: int = 3,
        n_depth: int = 2,
        bias: bool = True,
    ) -> BaseModel:
        """
        :param int nch_in: Number of input channels.
        :param int nch_out: Number of output channels.
        :param int latent_dim: Dimension of each embedding.
        :param float kld_weight: Scaling factor for KL-Divergence
            (default: 2.5e-4).
        :param int nch_feats: Number of channels in the first convolution layer
            (default: 32).
        :param int ksize: Convolution kernel size (default: 3).
        :param int n_depth: Number of convolution blocks, each reducing the
            input size by a factor of 2^n_depth (default: 2).
        :param bool bias: Whether to add bias in convolution operations
            (default: True).
        """
        super().__init__()
        self.kld_weight = kld_weight
        self.n_depth = n_depth

        c = nch_feats
        pad = ksize >> 1
        self.enc_0a = nn.Conv3d(nch_in, nch_feats, ksize, 1, pad, bias=bias)
        for i in range(n_depth + 1):
            if i > 0:
                self.register_module(
                    f"enc_{i}a", nn.Conv3d(c, c << 1, ksize, 1, pad, bias=bias)
                )
                c = c << 1
            self.register_module(
                f"enc_{i}b",
                nn.Conv3d(c, c if i < n_depth else c >> 1, ksize, 1, pad, bias=bias),
            )

        self.fc_mu = nn.Conv3d(c >> 1, latent_dim, 1, 1, 0, bias=bias)
        self.fc_var = nn.Conv3d(c >> 1, latent_dim, 1, 1, 0, bias=bias)
        self.dec_in = nn.Conv3d(latent_dim, c >> 1, 1, 1, 0, bias=bias)

        for i in reversed(range(n_depth)):
            self.register_module(
                f"dec_{i}a", nn.Conv3d(c, c >> 1, ksize, 1, pad, bias=bias)
            )
            c = c >> 1
            self.register_module(
                f"dec_{i}b",
                nn.Conv3d(c, c >> 1 if i > 0 else c, ksize, 1, pad, bias=bias),
            )

        self.conv = nn.Conv3d(nch_feats, nch_out, ksize, 1, pad, bias=bias)
        self.max_pooling = nn.MaxPool3d(kernel_size=2)
        self.upsample = lambda x, y: F.interpolate(x, y.shape[2:], mode="nearest")

    def forward(
        self, input: torch.Tensor, target: torch.Tensor = None
    ) -> Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        """See :class:`src.model.base.BaseModel`."""
        x = input
        skip = []
        for i in range(self.n_depth):
            x = F.relu_(self.get_submodule(f"enc_{i}a")(x))
            x = F.relu_(self.get_submodule(f"enc_{i}b")(x))
            skip.append(x)
            x = self.max_pooling(x)

        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}a")(x))
        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}b")(x))

        mu = self.fc_mu(x)
        if self.training:
            log_var = self.fc_var(x)
            std = torch.exp(0.5 * log_var)
            eps = torch.randn_like(std)
            z = eps * std + mu
        else:
            z = mu
        x = self.dec_in(z)

        for i in reversed(range(self.n_depth)):
            h = skip.pop()
            x = self.upsample(x, h)
            x = torch.cat([x, h], dim=1)
            x = F.relu_(self.get_submodule(f"dec_{i}a")(x))
            x = F.relu_(self.get_submodule(f"dec_{i}b")(x))

        output = self.conv(x)

        if target is not None:
            _mu = torch.flatten(mu, start_dim=1)
            _log_var = torch.flatten(log_var, start_dim=1)
            kld_loss = torch.mean(
                -0.5 * torch.sum(1 + _log_var - _mu**2 - _log_var.exp(), dim=1), dim=0
            )
            return output, F.mse_loss(output, target) + self.kld_weight * kld_loss
        else:
            return output
