from abc import ABCMeta, abstractmethod
from typing import Tuple, Union

import torch
from fvcore.common.config import CfgNode
from fvcore.common.registry import Registry
from torch import nn

MODEL_REGISTRY = Registry("MODEL")
MODEL_REGISTRY.__doc__ = """
Registry for all denoising models.

All registered objects must be callable and return an instance of :class:`BaseModel`.
The models can be identified by `cfg.model.name`, and the parameters to build the model
should be specified in the `cfg.model` configuration object.
"""


class BaseModel(nn.Module, metaclass=ABCMeta):
    """Abstract base class for denoising models."""

    def __init__(self) -> nn.Module:
        """The subclass can specify its own arguments for the `__init__` method."""
        super().__init__()

    @abstractmethod
    def forward(
        self, input: torch.Tensor, target: torch.Tensor = None
    ) -> Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        """
        Performs a forward pass of the `input` tensor through the model and returns the
        output tensor. If `target` is provided, computes the training loss using the
        given training samples, and returns both the output tensor and the computed loss
        tensor.

        :param torch.Tensor input: The input tensor to be passed through the model.
        :param torch.Tensor target: The target tensor for computing the training loss,
            defaults to None.
        :return Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]: If `target` is
            None, returns the output tensor. If `target` is provided, returns a tuple
            containing the output tensor and the computed loss tensor.
        """
        pass


def build_model(cfg: CfgNode) -> BaseModel:
    """
    Builds and returns a denoising model based on the provided configuration.

    The model is constructed using the name and parameters specified in the `cfg.model`
    section of the configuration.

    :param CfgNode cfg: Configuration object containing model specifications.
    :return BaseModel: The denoising model.
    """
    model_config = dict(cfg.model)
    model = MODEL_REGISTRY.get(model_config.pop("name"))(**model_config)
    assert isinstance(model, BaseModel)
    return model
