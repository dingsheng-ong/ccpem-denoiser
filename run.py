import argparse
import datetime
import logging
import os
import time
from collections.abc import Iterable

import mrcfile
import numpy as np
import torch
import torch.utils
import torch.utils.data
from fvcore.common.checkpoint import Checkpointer
from fvcore.common.config import CfgNode
from torch import distributed as dist

from src.util import Trainer, overlap_tile_inference, seed_all_rng, setup_logger


def setup(args: argparse.Namespace) -> CfgNode:
    """
    Build the configuration object with the given YAML file. Set up the environment for
    distributed training if multiple GPUs are detected, configure the logger to record
    training details, and seed the RNG for reproducibility.

    :param argparse.Namespace args: Command-line arguments for configuration.
    :return CfgNode: The configuration object.
    """
    cfg = CfgNode.load_cfg(open(args.config_file, "r"))
    cfg.merge_from_list(args.opts)

    force_cpu = cfg.get("train", None) and cfg.train.get("device", None) == "cpu"
    if force_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = ""

    # DDP setup
    local_rank = int(os.getenv("LOCAL_RANK", "0"))
    world_size = int(os.getenv("LOCAL_WORLD_SIZE", "1"))
    if world_size > 1:
        assert torch.cuda.is_available(), "no GPU"
        dist.init_process_group(backend="nccl", rank=local_rank, world_size=world_size)
        torch.cuda.set_device(local_rank)

    # skip setting up logger if --denoise-only is set
    if args.denoise_only or cfg.get("train", None) is None:
        return cfg

    setup_logger(cfg.train.output_dir, local_rank, name="denoise")
    config_save_path = os.path.join(cfg.train.output_dir, "config.yaml")
    with open(config_save_path, "w") as f:
        f.write(cfg.dump())
    logging.getLogger("denoise").info(f"save config to: {config_save_path}")

    seed = cfg.train.get("rng_seed", -1)
    seed_all_rng(None if seed is None else seed + local_rank)

    cfg.train.resume = args.resume
    return cfg


def denoise(cfg: CfgNode):
    """
    Run the denoising process using configurations defined in `cfg.denoise`.

    This function handles the denoising of tomographic maps as specified in the
    configuration. The `cfg.denoise` section should include the following keys:
    - `maps`: Path to the map(s) to be denoised. Can be a single map or a pair of
      half-maps.
    - `overlap_tile`: Boolean flag to determine whether to use the overlap-tile
      strategy.
    - `overlap_size`: Size of the overlap between tiles, applicable if `overlap_tile`
      is true.
    - `patch_size`: Size of each tile, applicable if `overlap_tile` is true.
    - `batch_size`: Batch size for processing tiles, applicable if `overlap_tile` is
      true.
    - `output`: Path to save the denoised map.

    :param CfgNode cfg: Configuration node containing denoising parameters.
    """
    rank = dist.get_rank() if dist.is_initialized() else 0

    model = Trainer.build_model(cfg)
    assert os.path.exists(cfg.denoise.model_path), "model not found"
    Checkpointer(model).resume_or_load(cfg.denoise.model_path)
    model.eval()

    if rank == 0:
        if isinstance(cfg.denoise.maps, str):
            inputs = mrcfile.read(cfg.denoise.maps)
            dtypes = [inputs.dtype]
            inputs = np.array(inputs, dtype=np.float32, copy=True)
            if cfg.denoise.normalise:
                mean, std, eps = (
                    np.mean(inputs),
                    np.std(inputs),
                    np.finfo(inputs.dtype).eps,
                )
                inputs = (inputs - mean) / max(std, eps)
            inputs = [inputs]
        elif isinstance(cfg.denoise.maps, Iterable):
            assert (
                len(cfg.denoise.maps) == 2
            ), f"expect two half-maps, got {len(cfg.denoise.maps)} file(s)"
            inputs = [mrcfile.read(x) for x in cfg.denoise.maps]
            dtypes = [x.dtype for x in inputs]
            inputs = [np.array(x, dtype=np.float32, copy=True) for x in inputs]
            if cfg.denoise.normalise:
                t = np.stack(inputs)
                mean, std, eps = t.mean(), t.std(), np.finfo(t.dtype).eps
                inputs = [(x - mean) / max(std, eps) for x in inputs]
        else:
            raise Exception(
                "unsupported type for denoise.maps;"
                f"expect str or list, got: {type(cfg.denoise.maps)}"
            )
    else:
        inputs = [None] if isinstance(cfg.denoise.maps, str) else [None, None]
        dtypes = [None] if isinstance(cfg.denoise.maps, str) else [None, None]

    output = 0
    for x, dtype in zip(inputs, dtypes):
        if rank == 0:
            x = torch.from_numpy(x).unsqueeze(0).unsqueeze(0).float()
        if cfg.denoise.get("overlap_tile", False):
            y = overlap_tile_inference(
                x,
                model,
                cfg.denoise.get("patch_size", 64),
                cfg.denoise.get("batch_size", 1),
                cfg.denoise.get("overlap_size", 4),
            )
        else:
            if rank == 0:
                if torch.cuda.is_available():
                    x = x.cuda()
                y = model(x)
            else:
                y = None  # do nothing in other GPUs
        if rank == 0:
            y = y[0, 0].detach().cpu().numpy().astype(dtype)
            output += y

    if rank == 0:
        output = output / len(inputs)
        with mrcfile.new(cfg.denoise.output, overwrite=True) as mrc:
            mrc.set_data(output)


def main(args: argparse.Namespace):
    cfg = setup(args)

    # skip training if --denoise-only is set or `train` is not defined in config.yaml
    if not args.denoise_only and cfg.get("train", None) is not None:
        logger = logging.getLogger("denoise")
        tic = time.perf_counter()
        trainer = Trainer(cfg)
        trainer.train()
        total_time = time.perf_counter() - tic
        logger.info(f"Total training time: {datetime.timedelta(seconds=total_time)}")

    # run denoising
    if cfg.get("denoise", None) is not None:
        tic = time.perf_counter()
        denoise(cfg)
        total_time = time.perf_counter() - tic
        if not dist.is_initialized() or dist.get_rank() == 0:
            print(f"Total denoising time: {datetime.timedelta(seconds=total_time)}")

    if dist.is_initialized() and dist.get_world_size() > 1:
        dist.destroy_process_group()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config-file", required=True, metavar="FILE", help="path to config file"
    )
    parser.add_argument(
        "--resume", action="store_true", help="whether to attempt to resume"
    )
    parser.add_argument(
        "--denoise-only",
        action="store_true",
        help="only run denoising with trained model",
    )
    parser.add_argument(
        "opts",
        default=None,
        nargs=argparse.REMAINDER,
        help="Modify config options using the command-line",
    )
    args = parser.parse_args()
    main(args)
